package run;

import context.mm_context;
import core.reactor.mm_start;
import log.mm_logger;

/**
 * 主入口
 * @author dongyazhuo
 */
public class mm_application {

	private static String TAG = mm_application.class.getSimpleName() + " : ";

	public static void main(String[] args) {

		// 初始化全局对象
		mm_context _mm_context = mm_context.instance();
		_mm_context.init(args);
		mm_logger.i(TAG + "mm_context init ok .");
		mm_start.instance().do_work();
		// TODO!!!屏蔽配置守护，问题有点多，功能未完善。
		// _mm_context.config_guard();
	}

}
