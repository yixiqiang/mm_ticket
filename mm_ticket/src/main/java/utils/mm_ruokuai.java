package utils;

import java.util.HashMap;

import com.alibaba.fastjson.JSON;

import config.mm_config;
import core.reactor.mm_reactor;
import core.reactor.mm_start;
import log.mm_logger;
import open.RuoKuai;

/**
 * 打码器
 * @author dongyazhuo
 */
public class mm_ruokuai {

	private mm_config _mm_config;

	public static final String code_file_login = "_code_login.png";
	public static final String code_file_order = "_code_order.png";

	private mm_reactor _mm_reactor;

	public mm_ruokuai(mm_reactor _mm_reactor) {
		super();
		this._mm_reactor = _mm_reactor;
		this._mm_config = _mm_reactor._mm_config;
	}

	public static String get_file_path(mm_config _mm_config, boolean is_login) {
		String user = _mm_config.getSet().getAccount_12306().getUser();
		return is_login ? user + code_file_login : user + code_file_order;
	}

	@SuppressWarnings("rawtypes")
	private char[] get_code_id(boolean is_login) {
		String user_name = _mm_config.getAuto_code_account().getUser();
		String pwd = _mm_config.getAuto_code_account().getPwd();
		String soft_key = "6facb9da7bb645ad9c4a229464b2cf89";

		String file_path = get_file_path(_mm_config, is_login);
		String r = RuoKuai.createByPost(user_name, pwd, "6113", "60", "96061", soft_key, file_path);
		HashMap reslut = JSON.parseObject(r, HashMap.class);
		if (null == reslut || null == reslut.get("Result")) {
			mm_logger.e("打码平台错误:请登录打码平台查看-http://www.ruokuai.com/client/index?6726   :" + reslut.get("Error"));
			// 增加配置文件实时更新的排除
			// TODO!!!屏蔽配置守护，问题有点多，功能未完善。
			// mm_context.instance()._mm_config_guard.exclude.put(user_name, user_name);
			mm_start.stop(_mm_reactor);
		}
		String value = reslut.get("Result").toString();
		return value.toCharArray();
	}

	public String get_code_xy(boolean is_login) {
		// 5108302
		char[] get_code_id = get_code_id(is_login);
		int x = 0, y = 0;

		String result = "";

		for (int i = 0; i < get_code_id.length; i++) {
			int s = Integer.parseInt(get_code_id[i] + "");
			// int s = 1;
			switch (s) {
			case 1:
				x = 40;
				y = 77;
				break;
			case 2:
				x = 112;
				y = 77;
				break;
			case 3:
				x = 184;
				y = 77;
				break;
			case 4:
				x = 256;
				y = 77;
				break;
			case 5:
				x = 40;
				y = 149;
				break;
			case 6:
				x = 40;
				y = 112;
				break;
			case 7:
				x = 184;
				y = 149;
				break;
			case 8:
				x = 256;
				y = 149;
				break;
			}
			if (result.length() > 0) {
				result += ",";
			}
			result += x + "," + y;
		}
		return result;
	}

}
