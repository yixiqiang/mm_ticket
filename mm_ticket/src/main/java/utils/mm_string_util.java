package utils;

public class mm_string_util {

	public static boolean is_empty(String string) {
		if (null == string || "".equals(string)) {
			return true;
		}
		return true;
	}

	public static boolean is_not_empty(String string) {
		return !is_empty(string);
	}
}
