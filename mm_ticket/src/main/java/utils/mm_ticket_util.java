package utils;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import context.mm_constant;
import core.query.mm_query_result;
import log.mm_logger;

/**
 * @author dongyazhuo
 */
public class mm_ticket_util {

	private static String TAG = mm_ticket_util.class.getSimpleName() + " : ";

	public static String get_queue_data_par_train_date(mm_query_result _mm_query_result) {
		Date parse = mm_date_util.parse(_mm_query_result.train_date);
		SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss", Locale.ENGLISH);
		return format.format(parse) + " GMT+0800 (中国标准时间)";
	}

	public static String get_seat_name(int seat) {
		Set<Entry<String, Integer>> entrySet = mm_constant.seat_conf.entrySet();
		for (Entry<String, Integer> entry : entrySet) {
			if (entry.getValue() == seat) {
				return entry.getKey();
			}
		}
		mm_logger.e(TAG + " un not found get_seat_name seat : " + seat);
		System.exit(0);
		return "";
	}

	public static String get_seat_commit_code(int seat) {
		String name = get_seat_name(seat);
		Set<Entry<String, String>> entrySet = mm_constant.passengerTicketStr.entrySet();
		for (Entry<String, String> entry : entrySet) {
			if (entry.getKey().equals(name)) {
				return entry.getValue();
			}
		}
		mm_logger.e(TAG + " un not found get_seat_commit_code seat : " + seat);
		System.exit(0);
		return "";
	}

	/**
	 * 获取参数配置
	 * @param args
	 * @return
	 */
	public static Map<String, String> get_config(String[] args) {
		if (args.length % 2 != 0) {
			mm_logger.e(TAG + " args error!!! " + Arrays.asList(args));
		}
		Map<String, String> result = new LinkedHashMap<>();
		if (null != args && args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				String key = args[i].trim();
				if (key.startsWith("-")) {
					key = key.substring(1, key.length());
					result.put(key, args[++i]);
				}
			}
		}
		if (null == result.get("c")) {
			result.put("c", "src/main/resources/ticket_config");
		}
		return result;
	}
}
