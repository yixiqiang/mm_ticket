package utils;

/**
 * @author dongyazhuo
 */
public class mm_os {

	public static boolean isWindows() {
		String os = System.getProperty("os.name");
		if (os != null)
			os = os.toLowerCase();
		return os != null && os.indexOf("windows") != -1;
	}

	public static boolean isLinux() {
		String os = System.getProperty("os.name");
		if (os != null)
			os = os.toLowerCase();
		return os != null && os.indexOf("linux") != -1;
	}
}
