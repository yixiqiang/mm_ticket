package utils;

/**
 * 线程
 * @author dongyazhuo
 */
public class mm_thread {

	private Thread thread = null;

	public Thread get_thread() {
		return thread;
	}

	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void start(Runnable runnable) {
		this.thread = new Thread(runnable);
		thread.start();
	}

	public void stop() {
		if (null != thread) {
			try {
				thread.stop();
			} catch (Exception e) {
			}

		}
	}

	/**
	 * 延迟启动
	 * @param runnable
	 * @param second 秒
	 */
	public void start(Runnable runnable, int second) {
		sleep(second * 1000);
		this.thread = new Thread(runnable);
		thread.start();
	}
}
