package utils;

public class mm_parse {

	public static String parse_str(Object value) {
		return null == value ? "" : value.toString();
	}

	public static Double parse_num(Object value) {
		return null == value ? 0.0D : Double.parseDouble(value.toString());
	}

	public static boolean parse_bool(Object value) {
		return null == value ? false : Boolean.parseBoolean(value.toString());
	}
}
