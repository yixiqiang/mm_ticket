package config;

/**
 * 自动打码的配置
 * @author dongyazhuo
 */
public class mm_config_auto_code_account {

	private String user;
	private String pwd;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

}
