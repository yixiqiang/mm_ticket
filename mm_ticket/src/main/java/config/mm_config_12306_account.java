package config;

/**
 * 12306配置账号密码
 * @author dongyazhuo
 */
public class mm_config_12306_account {

	private String user;
	private String pwd;
	private boolean query;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public boolean isQuery() {
		return query;
	}

	public void setQuery(boolean query) {
		this.query = query;
	}

}
