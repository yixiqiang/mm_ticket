package config;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import log.mm_logger;

/**
 * 车站信息表
 * @author dongyazhuo
 */
public class mm_config_station_name {

	private String TAG = mm_config_station_name.class.getSimpleName() + " : ";

	private volatile static mm_config_station_name _instance = null;

	private HashMap<String, String> station_table = new HashMap<>();

	private mm_config_station_name() {
	}

	static public mm_config_station_name instance() {
		if (null == _instance) {
			synchronized (mm_config_station_name.class) {
				if (null == _instance) {
					_instance = new mm_config_station_name();
				}
			}
		}
		return _instance;
	}

	public void init() {
		String file = read_file();
		file_fomart(file);
		mm_logger.i(TAG + " station table init finish  !!!");
	}

	private String read_file() {
		try {
			String encoding = "UTF-8";
			InputStream in = mm_config_station_name.class.getResourceAsStream("/station_name.txt");
			if (null != in) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(in, encoding);// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String result = bufferedReader.readLine();
				bufferedReader.close();
				read.close();
				return result;
			} else {
				mm_logger.e(TAG + " not found station table file  !!!");
			}
		} catch (Exception e) {
			mm_logger.e(TAG + " read station table file  error !!!");
			e.printStackTrace();
		}
		return null;
	}

	private void file_fomart(String content) {
		String[] table = content.split("=")[1].split("'")[1].split("@");
		for (int i = 1; i < table.length; i++) {
			String[] split = table[i].split("\\|");
			station_table.put(split[1], split[2]);
		}
	}

	/**
	 * 获取车站代号
	 * @param name
	 * @return
	 */
	public String get_station_by_name(String name) {
		if (station_table.size() == 0) {
			init();
		}
		return station_table.get(name);
	}

}
