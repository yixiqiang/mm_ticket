package config;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author dongyazhuo
 */
public class mm_config {

	private mm_config_set set;
	private int ticket_black_list_time;
	private boolean auto_code;
	private int auto_code_type;
	private mm_config_auto_code_account auto_code_account;
	private mm_config_email_conf email_conf;
	private int cdn;
	private int order_type;
	private int order_model;
	private String open_time;
	private int proxy;
	private LinkedHashMap<String, Map<String, Object>> url_config;
	private boolean start;

	public mm_config_set getSet() {
		return set;
	}

	public void setSet(mm_config_set set) {
		this.set = set;
	}

	public int getTicket_black_list_time() {
		return ticket_black_list_time;
	}

	public void setTicket_black_list_time(int ticket_black_list_time) {
		this.ticket_black_list_time = ticket_black_list_time;
	}

	public boolean isAuto_code() {
		return auto_code;
	}

	public void setAuto_code(boolean auto_code) {
		this.auto_code = auto_code;
	}

	public int getAuto_code_type() {
		return auto_code_type;
	}

	public void setAuto_code_type(int auto_code_type) {
		this.auto_code_type = auto_code_type;
	}

	public mm_config_auto_code_account getAuto_code_account() {
		return auto_code_account;
	}

	public void setAuto_code_account(mm_config_auto_code_account auto_code_account) {
		this.auto_code_account = auto_code_account;
	}

	public mm_config_email_conf getEmail_conf() {
		return email_conf;
	}

	public void setEmail_conf(mm_config_email_conf email_conf) {
		this.email_conf = email_conf;
	}

	public int getCdn() {
		return cdn;
	}

	public void setCdn(int cdn) {
		this.cdn = cdn;
	}

	public int getOrder_type() {
		return order_type;
	}

	public void setOrder_type(int order_type) {
		this.order_type = order_type;
	}

	public int getOrder_model() {
		return order_model;
	}

	public void setOrder_model(int order_model) {
		this.order_model = order_model;
	}

	public String getOpen_time() {
		return open_time;
	}

	public void setOpen_time(String open_time) {
		this.open_time = open_time;
	}

	public int getProxy() {
		return proxy;
	}

	public void setProxy(int proxy) {
		this.proxy = proxy;
	}

	public LinkedHashMap<String, Map<String, Object>> getUrl_config() {
		return url_config;
	}

	public void setUrl_config(LinkedHashMap<String, Map<String, Object>> url_config) {
		this.url_config = url_config;
	}

	public boolean isStart() {
		return start;
	}

	public void setStart(boolean start) {
		this.start = start;
	}

}
