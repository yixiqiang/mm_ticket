package config;

/**
 * 邮件配置
 * @author dongyazhuo
 */
public class mm_config_email_conf {

	private boolean open;
	private String email;
	private String notice_email;
	private String username;
	private String password;
	private String host;

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNotice_email() {
		return notice_email;
	}

	public void setNotice_email(String notice_email) {
		this.notice_email = notice_email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

}
