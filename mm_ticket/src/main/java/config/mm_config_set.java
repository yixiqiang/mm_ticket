package config;

import java.util.List;

public class mm_config_set {

	private List<String> station_dates;
	private boolean by_time;
	private List<String> train_types;
	private String departure_time;
	private String arrival_time;
	private String take_time;
	private List<String> station_trains;
	private String from_station;
	private String to_station;
	private List<String> set_type;
	private boolean more_ticket;
	private List<String> ticke_peoples;
	private mm_config_12306_account account_12306;

	public List<String> getStation_dates() {
		return station_dates;
	}

	public void setStation_dates(List<String> station_dates) {
		this.station_dates = station_dates;
	}

	public boolean isBy_time() {
		return by_time;
	}

	public void setBy_time(boolean by_time) {
		this.by_time = by_time;
	}

	public List<String> getTrain_types() {
		return train_types;
	}

	public void setTrain_types(List<String> train_types) {
		this.train_types = train_types;
	}

	public String getDeparture_time() {
		return departure_time;
	}

	public void setDeparture_time(String departure_time) {
		this.departure_time = departure_time;
	}

	public String getArrival_time() {
		return arrival_time;
	}

	public void setArrival_time(String arrival_time) {
		this.arrival_time = arrival_time;
	}

	public String getTake_time() {
		return take_time;
	}

	public void setTake_time(String take_time) {
		this.take_time = take_time;
	}

	public List<String> getStation_trains() {
		return station_trains;
	}

	public void setStation_trains(List<String> station_trains) {
		this.station_trains = station_trains;
	}

	public String getFrom_station() {
		return from_station;
	}

	public void setFrom_station(String from_station) {
		this.from_station = from_station;
	}

	public String getTo_station() {
		return to_station;
	}

	public void setTo_station(String to_station) {
		this.to_station = to_station;
	}

	public List<String> getSet_type() {
		return set_type;
	}

	public void setSet_type(List<String> set_type) {
		this.set_type = set_type;
	}

	public boolean isMore_ticket() {
		return more_ticket;
	}

	public void setMore_ticket(boolean more_ticket) {
		this.more_ticket = more_ticket;
	}

	public List<String> getTicke_peoples() {
		return ticke_peoples;
	}

	public void setTicke_peoples(List<String> ticke_peoples) {
		this.ticke_peoples = ticke_peoples;
	}

	public mm_config_12306_account getAccount_12306() {
		return account_12306;
	}

	public void setAccount_12306(mm_config_12306_account account_12306) {
		this.account_12306 = account_12306;
	}

}
