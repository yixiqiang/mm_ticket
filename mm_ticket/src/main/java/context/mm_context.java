package context;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import config.mm_config;
import core.net.mm_cdn;
import core.net.mm_http_client;
import log.mm_logger;
import log.mm_logger_callback_printf;
import utils.mm_thread;
import utils.mm_ticket_util;

/**
 * 上下文对象
 * @author dongyazhuo
 * @param <K>
 */

public class mm_context {

	private String TAG = mm_context.class.getSimpleName() + " ";

	private LinkedHashMap<String, mm_config> user_config = new LinkedHashMap<>();// 用户配置文件

	private volatile static mm_context _instance = null;

	public mm_config_guard _mm_config_guard = null;

	private Map<String, String> args_map = new LinkedHashMap<String, String>();

	private mm_context() {
	}

	static public mm_context instance() {
		if (null == _instance) {
			synchronized (mm_context.class) {
				if (null == _instance) {
					_instance = new mm_context();
				}
			}
		}
		return _instance;
	}

	private Yaml yaml = new Yaml();

	private mm_cdn mm_cdn = null;

	public void init(String[] args) {
		// 初始化日志文件
		mm_logger.assign_call_back(new mm_logger_callback_printf());

		args_map = mm_ticket_util.get_config(args);
		// 初始化用户配置
		init_user_config(args_map);

		// 启动cdn测试
		cdn_test();
	}

	public void config_guard() {
		// 启动配置守护
		_mm_config_guard = new mm_config_guard(args_map.get("c"));
		new mm_thread().start(_mm_config_guard);
	}

	private void init_user_config(Map<String, String> args_map) {
		String path = args_map.get("c");

		if (null == path) {
			mm_logger.e(TAG + "config path error !!!");
			System.exit(0);
		}

		File file = new File(path);
		if (!file.exists()) {
			mm_logger.e(TAG + "config path error !!!");
			System.exit(0);
		}

		File[] tempList = new File(path).listFiles();
		for (int i = 0; i < tempList.length; i++) {
			if (tempList[i].isFile()) {
				String resource = path + "\\" + tempList[i].getName();
				try {
					FileInputStream stream = new FileInputStream(new File(resource));
					mm_config config = yaml.loadAs(stream, mm_config.class);
					config.setUrl_config(get_url_config());
					// if (config.isStart()) {
					user_config.put(resource, config);
					// }
					stream.close();
				} catch (Exception e) {
					mm_logger.w(TAG + " config file error ,please check!!! " + resource);
				}
			}
		}
		if (user_config.size() == 0) {
			mm_logger.e(TAG + "the normal config file not found !!!");
			System.exit(0);
		}
	}

	private void cdn_test() {
		LinkedHashMap<String, Map<String, Object>> url_config = get_url_config();
		mm_cdn = new mm_cdn(new mm_http_client(), url_config.get("loginInitCdn"));
		new mm_thread().start(mm_cdn);
	}

	private LinkedHashMap<String, Map<String, Object>> get_url_config() {
		// 如果读入Map,这里可以是Mapj接口,默认实现为LinkedHashMap
		InputStream inputStream = mm_config.class.getResourceAsStream("/url_config.yml");
		LinkedHashMap<String, Map<String, Object>> url_config = yaml.loadAs(inputStream, LinkedHashMap.class);
		return url_config;
	}

	public LinkedHashMap<String, mm_config> get_configs() {
		return user_config;
	}

	public void set_configs(String key, mm_config _mm_config) {
		user_config.put(key, _mm_config);
	}

	public String get_cdn() {
		return mm_cdn.get_cdn();
	}

}
