package context;

import java.util.LinkedHashMap;

/**
 * 常量池
 * @author dongyazhuo
 */
public class mm_constant {
	public static String QUERY_C = "查询到有余票，尝试提交订单";
	public static String QUERY_IN_BLACK_LIST = "该车次{} 正在被关小黑屋，跳过此车次";

	public static int SUCCESS_CODE = 000000;
	public static int FAIL_CODE = 999999;
	public static String AUTO_SUBMIT_ORDER_REQUEST_C = "提交订单成功";
	public static String AUTO_SUBMIT_ORDER_REQUEST_F = "提交订单失败，重新刷票中";
	public static String AUTO_SUBMIT_NEED_CODE = "需要验证码";
	public static String AUTO_SUBMIT_NOT_NEED_CODE = "不需要验证码";

	public static int TICKET_BLACK_LIST_TIME = 5; // 加入小黑屋的等待时间，默认5 min

	public static String DTO_NOT_FOUND = "未查找到常用联系人, 请查证后添加!!";
	public static String DTO_NOT_IN_LIST = "联系人不在列表中，请查证后添加!!";

	public static String QUEUE_TICKET_SHORT = "当前余票数小于乘车人数，放弃订票";
	public static String QUEUE_TICKET_SUCCESS = "排队成功, 当前余票还剩余: {0}张";
	public static String QUEUE_JOIN_BLACK = "排队发现未知错误{0}，将此列车 {1}加入小黑屋";
	public static String QUEUE_WARNING_MSG = "排队异常，错误信息：{0}, 将此列车 {1}加入小黑屋";

	public static int OUT_NUM = 120; // 排队请求12306的次数;
	public static String WAIT_OUT_NUM = "超出排队时间，自动放弃，正在重新刷票";
	public static String WAIT_ORDER_SUCCESS = "恭喜您订票成功，订单号为：{%s}, 请立即打开浏览器登录12306，访问‘未完成订单’，在30分钟内完成支付!";
	public static String WAIT_ORDER_CONTINUE = "排队等待时间预计还剩 {%s} s";
	public static String WAIT_ORDER_FAIL = "排队等待失败，错误消息：{%s}";
	public static String WAIT_ORDER_NUM = "第{%d}次排队中,请耐心等待";
	public static String WAIT_ORDER_SUB_FAIL = "订单提交失败！,正在重新刷票";

	public static String CANCEL_ORDER_SUCCESS = "排队超时，已为您自动取消订单，订单编号: {0}";
	public static String CANCEL_ORDER_FAIL = "排队超时，取消订单失败， 订单号{0}";

	public static String REST_TIME = "12306休息时间，本程序自动停止,明天早上6点将自动运行";
	public static String REST_TIME_PAST = "休息时间已过，重新开启检票功能";
	public static String LOGIN_SESSION_FAIL = "用户检查失败：%s，可能未登录，可能session已经失效, 正在重新登录中";

	public static final LinkedHashMap<String, Integer> seat_conf = new LinkedHashMap<String, Integer>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			this.put("商务座", 32);
			this.put("一等座", 31);
			this.put("二等座", 30);
			this.put("特等座", 25);
			this.put("软卧", 23);
			this.put("硬卧", 28);
			this.put("动卧", 33);
			this.put("软座", 24);
			this.put("硬座", 29);
			this.put("无座", 26);
		}
	};

	public static final LinkedHashMap<String, String> passengerTicketStr = new LinkedHashMap<String, String>() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			this.put("商务座", "9");
			this.put("一等座", "M");
			this.put("二等座", "O");
			this.put("特等座", "P");
			this.put("软卧", "4");
			this.put("硬卧", "3");
			// this.put("动卧", 33);
			this.put("软座", "2");
			this.put("硬座", "1");
			this.put("无座", "1");
		}
	};
}
