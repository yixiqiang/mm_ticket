package log;

/**
 * 默认的日志实现
 * @author dongyazhuo
 */
public class mm_logger_interface_default_impl implements mm_logger_interface {

	/**
	 * 日志输出
	 */
	@Override
	public void event_logger(int lvl, String message) {
		synchronized (mm_logger.class) {
			mm_logger_mark lm = mm_logger_mark.level_mark_by_lvl(lvl);
			System.out.println(" " + lvl + " " + lm.mark + " " + lm.name + " " + message);
		}
	}

}
