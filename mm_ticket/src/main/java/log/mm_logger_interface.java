package log;

/**
 * 日志接口
 * @author dongyazhuo
 */
public interface mm_logger_interface {
	/**
	 * 日志输出
	 * @param lvl
	 * @param message
	 */
	public void event_logger(int lvl, String message);
}
