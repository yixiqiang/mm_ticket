package log;

import java.text.SimpleDateFormat;
import java.util.Date;

public class mm_logger_callback_printf implements mm_logger_interface {
	public SimpleDateFormat data_format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss SSS");

	@Override
	public void event_logger(int lvl, String message) {
		synchronized (mm_logger.class) {
			// [1992/01/26 09:13:14-520 8 V ]
			Date time_now = new Date();
			// [ 8 V ]
			mm_logger_mark lm = mm_logger_mark.level_mark_by_lvl(lvl);
			System.out.println(data_format.format(time_now) + " " + lvl + " " + lm.mark + " " + message);
		}
	}
}
