package log;

/**
 * 日志类
 * @author dongyazhuo
 */
public class mm_logger {

	public mm_logger_interface logger = new mm_logger_interface_default_impl();
	private volatile static mm_logger _instance = null;

	public static mm_logger instance() {
		if (null == _instance) {
			synchronized (mm_logger.class) {
				if (null == _instance) {
					_instance = new mm_logger();
				}
			}
		}
		return _instance;
	}

	/**
	 * 更改日志实现
	 * @param callback
	 */
	public static void assign_call_back(mm_logger_interface mm_logger) {
		// android assertions are unreliable.
		// assert null != callback : "you can not assign null callback.";
		mm_logger g_logger = instance();
		synchronized (g_logger) {
			g_logger.logger = mm_logger;
		}
	}

	/**
	 * 未知日志
	 * @param message
	 */
	public static void u(String message) {
		mm_logger mm_logger = instance();
		mm_logger.logger.event_logger(mm_logger_const.MM_LOG_UNKNOW, message);
	}

	static public void f(String message) {
		mm_logger g_logger = mm_logger.instance();
		g_logger.logger.event_logger(mm_logger_const.MM_LOG_FATAL, message);
	}

	static public void c(String message) {
		mm_logger g_logger = mm_logger.instance();
		g_logger.logger.event_logger(mm_logger_const.MM_LOG_CRIT, message);
	}

	static public void e(String message) {
		mm_logger g_logger = mm_logger.instance();
		g_logger.logger.event_logger(mm_logger_const.MM_LOG_ERROR, message);
	}

	static public void a(String message) {
		mm_logger g_logger = mm_logger.instance();
		g_logger.logger.event_logger(mm_logger_const.MM_LOG_ALERT, message);
	}

	static public void w(String message) {
		mm_logger g_logger = mm_logger.instance();
		g_logger.logger.event_logger(mm_logger_const.MM_LOG_WARNING, message);
	}

	static public void n(String message) {
		mm_logger g_logger = mm_logger.instance();
		g_logger.logger.event_logger(mm_logger_const.MM_LOG_NOTICE, message);
	}

	static public void i(String message) {
		mm_logger g_logger = mm_logger.instance();
		g_logger.logger.event_logger(mm_logger_const.MM_LOG_INFO, message);
	}

	static public void t(String message) {
		mm_logger g_logger = mm_logger.instance();
		g_logger.logger.event_logger(mm_logger_const.MM_LOG_TRACE, message);
	}

	static public void d(String message) {
		mm_logger g_logger = mm_logger.instance();
		g_logger.logger.event_logger(mm_logger_const.MM_LOG_DEBUG, message);
	}

	static public void v(String message) {
		mm_logger g_logger = mm_logger.instance();
		g_logger.logger.event_logger(mm_logger_const.MM_LOG_VERBOSE, message);
	}

}
