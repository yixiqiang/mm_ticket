package log;

import java.util.HashMap;

/**
 * 日志常量
 * @author dongyazhuo
 */
public class mm_logger_const {

	public static final int MM_LOG_UNKNOW = 0;// 未知的
	public static final int MM_LOG_FATAL = 1; // 失败
	public static final int MM_LOG_CRIT = 2;
	public static final int MM_LOG_ERROR = 3; // 错误
	public static final int MM_LOG_ALERT = 4; // 提醒
	public static final int MM_LOG_WARNING = 5; // 警告
	public static final int MM_LOG_NOTICE = 6; // 通知
	public static final int MM_LOG_INFO = 7; // 标准信息
	public static final int MM_LOG_TRACE = 8; // 痕迹追踪
	public static final int MM_LOG_DEBUG = 9; // 调试
	public static final int MM_LOG_VERBOSE = 10; // 冗余的

	/**
	 * 常量标记
	 */
	public static HashMap<Integer, mm_logger_mark> const_level_mark = new HashMap<Integer, mm_logger_mark>() {
		private static final long serialVersionUID = 1L;

		{
			this.put(MM_LOG_UNKNOW, new mm_logger_mark("U", "unknow"));
			this.put(MM_LOG_FATAL, new mm_logger_mark("F", "fatal"));
			this.put(MM_LOG_CRIT, new mm_logger_mark("C", "crit"));
			this.put(MM_LOG_ERROR, new mm_logger_mark("E", "error"));
			this.put(MM_LOG_ALERT, new mm_logger_mark("A", "alert"));
			this.put(MM_LOG_WARNING, new mm_logger_mark("W", "warning"));
			this.put(MM_LOG_NOTICE, new mm_logger_mark("N", "notice"));
			this.put(MM_LOG_INFO, new mm_logger_mark("I", "info"));
			this.put(MM_LOG_TRACE, new mm_logger_mark("T", "trace"));
			this.put(MM_LOG_DEBUG, new mm_logger_mark("D", "debug"));
			this.put(MM_LOG_VERBOSE, new mm_logger_mark("V", "verbose"));
		}
	};
}
