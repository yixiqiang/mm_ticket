package log;

/**
 * 日志标记
 * @author dongyazhuo
 */
public class mm_logger_mark {

	public String mark; // 标记
	public String name; // 名字

	public mm_logger_mark(String mark, String name) {
		this.mark = mark;
		this.name = name;
	}

	/**
	 * 获取日志标记
	 * @param lvl
	 * @return
	 */
	public static mm_logger_mark level_mark_by_lvl(int lvl) {
		if (mm_logger_const.MM_LOG_FATAL <= lvl && mm_logger_const.MM_LOG_VERBOSE >= lvl) {
			return mm_logger_const.const_level_mark.get(lvl);
		} else {
			return mm_logger_const.const_level_mark.get(mm_logger_const.MM_LOG_UNKNOW);
		}
	}
}
