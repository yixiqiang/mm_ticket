package core.commit;

import java.util.HashMap;

import core.query.mm_query_result;

/**
 * 提交器
 * @author dongyazhuo
 */
public interface mm_submit {

	public void submit(HashMap<String, Object> request_parm, mm_query_result _mm_query_result);

}
