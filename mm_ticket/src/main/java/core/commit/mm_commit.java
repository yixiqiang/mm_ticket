package core.commit;

import java.util.HashMap;

import config.mm_config;
import core.commit.auto.mm_auto_submit;
import core.commit.param_constructor.mm_commit_param_constructor;
import core.commit.standard.mm_standard_submit;
import core.query.mm_query_result;
import core.reactor.mm_reactor;

/**
 * 订单提交 订单提交分为三个阶段 : 1,预定,2,排队,3,确认
 * @author dongyazhuo
 */
public class mm_commit {

	private mm_config _mm_config;
	private mm_reactor _mm_reactor;

	public mm_commit(mm_reactor _mm_reactor) {
		super();
		this._mm_reactor = _mm_reactor;
		this._mm_config = _mm_reactor._mm_config;
	}

	public void commit(mm_query_result _mm_query_result) {
		// 构造提交条件
		HashMap<String, Object> request_parm = mm_commit_param_constructor.commit_data_par(_mm_query_result, _mm_reactor);
		// 提交
		int order_type = _mm_config.getOrder_type();
		mm_submit _mm_submit = null;
		if (1 == order_type) {// 快读订单
			_mm_submit = new mm_auto_submit(_mm_reactor);
		} else {// 普通订单
			_mm_submit = new mm_standard_submit(_mm_reactor);
		}
		_mm_submit.submit(request_parm, _mm_query_result);
	}

}
