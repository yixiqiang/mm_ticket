package core.commit.param_constructor;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import core.query.mm_query_result;
import core.reactor.mm_reactor;
import utils.mm_ticket_util;

/**
 * 自动提交请求参数构造器
 * @author dongyazhuo
 */
public class mm_auto_commit_param_constructor {

	/**
	 * 自动提交的请求参数构造器
	 * @param request_parm
	 * @param _mm_query_result
	 * @param _mm_reactor
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static Map<String, Object> submit_data_par(HashMap<String, Object> request_parm,
			mm_query_result _mm_query_result, mm_reactor _mm_reactor) {
		// - secretStr 车票代码
		// - train_date 乘车日期
		// - tour_flag 乘车类型
		// - purpose_codes 学生还是成人
		// - query_from_station_name 起始车站
		// - query_to_station_name 结束车站
		// - cancel_flag 默认2，我也不知道干嘛的
		// - bed_level_order_num 000000000000000000000000000000
		// - passengerTicketStr 乘客乘车代码
		// - oldPassengerStr 乘客编号代码
		Map<String, Object> param = new LinkedHashMap<String, Object>();
		param.put("secretStr", URLDecoder.decode(_mm_query_result.secretStr));
		param.put("train_date", _mm_query_result.train_date);
		param.put("tour_flag", "dc");
		param.put("purpose_codes", "ADULT");
		param.put("query_from_station_name", _mm_reactor._mm_config.getSet().getFrom_station());
		param.put("query_to_station_name", _mm_reactor._mm_config.getSet().getTo_station());
		param.put("cancel_flag", 2);
		param.put("bed_level_order_num", "000000000000000000000000000000");
		param.put("passengerTicketStr", request_parm.get("passengerTicketStr"));
		param.put("oldPassengerStr", request_parm.get("oldPassengerStr"));
		return param;
	}

	/**
	 * 自动提交的排队参数构造
	 * @return
	 */
	public static Map<String, Object> submit_queue_data_par(mm_query_result _mm_query_result) {
		// - 字段说明
		// - train_date 时间
		// - train_no 列车编号,查询代码里面返回
		// - stationTrainCode 列车编号
		// - seatType 对应坐席
		// - fromStationTelecode 起始城市
		// - toStationTelecode 到达城市
		// - leftTicket 查询代码里面返回
		// - purpose_codes 学生还是成人
		// - _json_att 没啥卵用，还是带上吧
		String train_date = mm_ticket_util.get_queue_data_par_train_date(_mm_query_result);
		String seatType = mm_ticket_util.get_seat_commit_code(_mm_query_result.seat);
		Map<String, Object> param = new LinkedHashMap<String, Object>();
		param.put("train_date", train_date);
		param.put("train_no", _mm_query_result.train_no);
		param.put("stationTrainCode", _mm_query_result.stationTrainCode);
		param.put("seatType", seatType);
		param.put("fromStationTelecode", _mm_query_result.query_from_station_name);
		param.put("toStationTelecode", _mm_query_result.query_to_station_name);
		param.put("leftTicket", _mm_query_result.leftTicket);
		param.put("purpose_codes", "ADULT");
		param.put("_json_att", "");
		return param;
	}

	/**
	 * 自动提交，排队完成后，需要确认订单提交的构造器
	 * @param request_parm
	 * @param result_auto_submit
	 * @return
	 */
	public static Map<String, Object> submit_confirm_data_par(HashMap<String, Object> request_parm,
			String auto_submit_result) {
		// passengerTicketStr 乘客乘车代码
		// oldPassengerStr 乘客编号代码
		// randCode 填空
		// purpose_codes 学生还是成人
		// key_check_isChange autoSubmitOrderRequest返回的result字段做切割即可
		// leftTicketStr autoSubmitOrderRequest返回的result字段做切割即可
		// train_location autoSubmitOrderRequest返回的result字段做切割即可
		// choose_seats
		// seatDetailType
		// _json_att
		String[] split = auto_submit_result.split("#");
		Map<String, Object> param = new LinkedHashMap<String, Object>();
		param.put("passengerTicketStr", request_parm.get("passengerTicketStr"));
		param.put("oldPassengerStr", request_parm.get("oldPassengerStr"));
		param.put("randCode", "");
		param.put("purpose_codes", "ADULT");
		param.put("key_check_isChange", split[1]);
		param.put("leftTicketStr", split[2]);
		param.put("train_location", split[0]);
		param.put("choose_seats", "");
		param.put("seatDetailType", "");
		param.put("_json_att", "");
		return param;
	}

}
