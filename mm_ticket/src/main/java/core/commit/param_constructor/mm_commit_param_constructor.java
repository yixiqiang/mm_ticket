package core.commit.param_constructor;

import java.util.HashMap;
import java.util.Set;

import context.mm_constant;
import core.query.mm_query_result;
import core.reactor.mm_reactor;
import log.mm_logger;
import utils.mm_ticket_util;

/**
 * 提交的参数
 * @author dongyazhuo
 */
public class mm_commit_param_constructor {

	private static String TAG = mm_commit_param_constructor.class.getSimpleName() + " : ";

	public static HashMap<String, Object> commit_data_par(mm_query_result _mm_query_result,
			mm_reactor _mm_reactor) {

		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("status", false);

		if (_mm_query_result.status) {
			// if (_mm_cache.get(_mm_cache.train_no) > 0 || _mm_cache.get(_mm_cache.train_no) == -1) {
			// mm_logger.i(TAG + mm_constant.QUEUE_WARNING_MSG + _mm_query_result.train_no);
			// return result;
			// }
			StringBuilder passengerTicketStrList = new StringBuilder();
			StringBuilder oldPassengerStr = new StringBuilder();
			// 缓存联系人
			// mm_logger.i(TAG + mm_constant.QUERY_C);
			Set<HashMap<String, Object>> user_list = _mm_reactor._mm_address_book.get_user_list(false);
			String set_type = mm_ticket_util.get_seat_commit_code(_mm_query_result.seat);
			if (null == user_list || user_list.size() == 0) {
				String user = _mm_reactor._mm_config.getSet().getAccount_12306().getUser();
				mm_logger.e(TAG + user + " " + mm_constant.DTO_NOT_IN_LIST);
			}
			// 如果乘车人填错了导致没有这个乘车人的话，可能乘车人数会小于自动乘车人
			if (user_list.size() < _mm_query_result.is_more_ticket_num) {
				_mm_query_result.is_more_ticket_num = user_list.size();
			}
			if (_mm_query_result.is_more_ticket_num == 1) {
				HashMap<String, Object> user = get_set_item(user_list);
				passengerTicketStrList.append("0").append(",")
						.append(user.get("passenger_type")).append(",")
						.append(user.get("passenger_name")).append(",")
						.append(user.get("passenger_id_type_code")).append(",")
						.append(user.get("passenger_id_no")).append(",")
						.append(user.get("mobile_no")).append(",").append("N");
				oldPassengerStr.append(user.get("passenger_name")).append(",")
						.append(user.get("passenger_id_type_code")).append(",")
						.append(user.get("passenger_id_no")).append(",")
						.append(user.get("passenger_type")).append("_");

			} else {
				int a = 0;
				for (HashMap<String, Object> user : user_list) {
					if (a > 0) {
						passengerTicketStrList.append(",");
						oldPassengerStr.append(",");
					}
					passengerTicketStrList.append("0").append(",")
							.append(user.get("passenger_type")).append(",")
							.append(user.get("passenger_name")).append(",")
							.append(user.get("passenger_id_type_code")).append(",")
							.append(user.get("passenger_id_no")).append(",")
							.append(user.get("mobile_no")).append(",").append("N_" + set_type);
					oldPassengerStr.append(user.get("passenger_name")).append(",")
							.append(user.get("passenger_id_type_code")).append(",")
							.append(user.get("passenger_id_no")).append(",")
							.append(user.get("passenger_type")).append("_");
					a++;
				}
			}
			String passengerTicketStr = set_type + "," + passengerTicketStrList;
			String seat_type_str = "_" + mm_ticket_util.get_seat_commit_code(_mm_query_result.seat);
			if (passengerTicketStr.endsWith(seat_type_str)) {
				passengerTicketStr = passengerTicketStr.substring(0, passengerTicketStr.length() - seat_type_str.length());
			}

			result.put("passengerTicketStr", passengerTicketStr);
			result.put("oldPassengerStr", oldPassengerStr);
			result.put("code", mm_constant.SUCCESS_CODE);
			result.put("set_type", set_type);
			result.put("status", true);
			result.put("user_info", user_list);
			return result;
		}
		return result;
	}

	private static <T> T get_set_item(Set<T> s) {
		for (T t : s) {
			return t;
		}
		return null;
	}
}
