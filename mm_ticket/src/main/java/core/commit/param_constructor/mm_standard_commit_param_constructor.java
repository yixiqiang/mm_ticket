package core.commit.param_constructor;

import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;

import core.query.mm_query_result;
import utils.mm_date_util;
import utils.mm_ticket_util;

/**
 * 标准提交请求参数构造器
 * @author dongyazhuo
 */
public class mm_standard_commit_param_constructor {

	/**
	 * 标准提交预定按钮
	 * @param _mm_query_result
	 * @return
	 */
	public static Map<String, Object> submit_data_par(mm_query_result _mm_query_result) {
		// ('secretStr', self.secretStr), # 字符串加密
		// ('train_date', self.train_date), # 出发时间
		// ('back_train_date', time()), # 返程时间
		// ('tour_flag', 'dc'), # 旅途类型
		// ('purpose_codes', 'ADULT'), # 成人票还是学生票
		// ('query_from_station_name', self.from_station), # 起始车站
		// ('query_to_station_name', self.to_station), # 终点车站
		Map<String, Object> param = new LinkedHashMap<String, Object>();
		param.put("secretStr", URLDecoder.decode(_mm_query_result.secretStr));
		param.put("train_date", _mm_query_result.train_date);
		param.put("back_train_date", get_today());
		param.put("tour_flag", "dc");
		param.put("purpose_codes", "ADULT");
		param.put("query_from_station_name", _mm_query_result.query_from_station_name);
		param.put("query_to_station_name", _mm_query_result.query_to_station_name);
		return param;
	}

	/**
	 * 订单检查的参数构造
	 * @param request_parm
	 * @param token
	 * @return
	 */
	public static Map<String, Object> check_order_data_apr(HashMap<String, Object> request_parm, Map<String, Object> token,
			mm_query_result _mm_query_result) {
		Map<String, Object> param = new LinkedHashMap<String, Object>();
		param.put("passengerTicketStr", request_parm.get("passengerTicketStr"));
		param.put("oldPassengerStr", request_parm.get("oldPassengerStr"));
		param.put("REPEAT_SUBMIT_TOKEN", token.get("token"));
		param.put("randCode", "");
		param.put("cancel_flag", 2);
		param.put("bed_level_order_num", "000000000000000000000000000000");
		param.put("tour_flag", "dc");
		param.put("_json_att", "");
		return param;
	}

	/**
	 * 查询当前列车排队人数
	 * @param _mm_query_result
	 * @param token
	 * @return
	 */
	public static Map<String, Object> query_queue_count_data_par(mm_query_result _mm_query_result, Map<String, Object> token) {
		String train_date = mm_ticket_util.get_queue_data_par_train_date(_mm_query_result);
		HashMap<String, Object> ticketInfoForPassengerForm = (HashMap<String, Object>) token.get("ticketInfoForPassengerForm");
		HashMap<String, Object> queryLeftTicketRequestDTO = JSON.parseObject(
				ticketInfoForPassengerForm.get("queryLeftTicketRequestDTO").toString(),
				HashMap.class);
		Map<String, Object> param = new LinkedHashMap<String, Object>();
		param.put("train_date", train_date);
		param.put("train_no", queryLeftTicketRequestDTO.get("train_no"));
		param.put("stationTrainCode", queryLeftTicketRequestDTO.get("station_train_code"));
		param.put("seatType", mm_ticket_util.get_seat_commit_code(_mm_query_result.seat));
		param.put("fromStationTelecode", queryLeftTicketRequestDTO.get("from_station"));
		param.put("toStationTelecode", queryLeftTicketRequestDTO.get("to_station"));
		param.put("leftTicket", ticketInfoForPassengerForm.get("leftTicketStr"));
		param.put("purpose_codes", ticketInfoForPassengerForm.get("purpose_codes"));
		param.put("train_location", ticketInfoForPassengerForm.get("train_location"));
		param.put("REPEAT_SUBMIT_TOKEN", token.get("token"));
		return param;
	}

	/**
	 * 确认购买的请求参数构造
	 * @param _mm_query_result
	 * @return
	 */
	public static Map<String, Object> submit_confirm_data_par(mm_query_result _mm_query_result,
			HashMap<String, Object> assenger_result, Map<String, Object> token) {
		HashMap<String, Object> ticketInfoForPassengerForm = (HashMap<String, Object>) token.get("ticketInfoForPassengerForm");
		Map<String, Object> param = new LinkedHashMap<String, Object>();
		param.put("passengerTicketStr", assenger_result.get("passengerTicketStr"));
		param.put("oldPassengerStr", assenger_result.get("oldPassengerStr"));
		param.put("purpose_codes", ticketInfoForPassengerForm.get("purpose_codes"));
		param.put("key_check_isChange", ticketInfoForPassengerForm.get("key_check_isChange"));
		param.put("leftTicketStr", ticketInfoForPassengerForm.get("leftTicketStr"));
		param.put("train_location", ticketInfoForPassengerForm.get("train_location"));
		param.put("seatDetailType", "");// 开始需要选择座位，但是目前12306不支持自动选择作为，那这个参数为默认
		param.put("roomType", "00");// 好像是根据一个id来判断选中的，两种 第一种是00，第二种是10，但是我在12306的页面没找到该id，目前写死是00，不知道会出什么错
		param.put("dwAll", "N");
		param.put("whatsSelect", 1);
		param.put("_json_at", "");
		param.put("randCode", "");
		param.put("choose_seats", "");
		param.put("REPEAT_SUBMIT_TOKEN", token.get("token"));
		return param;
	}

	private static String get_today() {
		return mm_date_util.format(new Date(), mm_date_util.DEFAULT_DATE_PATTERN);
	}
}
