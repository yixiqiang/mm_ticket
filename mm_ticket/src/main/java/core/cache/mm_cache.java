package core.cache;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 缓存
 * @author dongyazhuo
 */
public class mm_cache {
	@SuppressWarnings("unused")
	private String TAG = mm_cache.class.getSimpleName() + " : ";

	private HashMap<Object, Long> cache = new HashMap<>();

	public Object user_time = new Object();

	public Object train_no = new Object();

	public ArrayList<Object> normal_passengers = null;

	/**
	 * 0:不存在 -1:永久 >0:剩余时长
	 * @param obj
	 * @return
	 */
	public synchronized long get(Object obj) {
		Long L = cache.get(obj);
		if (null == L) {
			return 0;
		} else if (-1L == L) {
			return -1L;
		} else {
			return L - System.currentTimeMillis();
		}
	}

	/**
	 * -1:永久 >0:剩余时长
	 * @param obj
	 * @return
	 */
	public synchronized void set(Object obj, long time) {
		long current = time == -1L ? 0 : System.currentTimeMillis();
		cache.put(obj, current + time);
	}

}
