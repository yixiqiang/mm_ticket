package core.login;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;

import config.mm_config;
import context.mm_constant;
import core.cache.mm_cache;
import core.reactor.mm_reactor;
import core.reactor.mm_start;
import log.mm_logger;
import utils.mm_parse;
import utils.mm_thread;

/**
 * 登陆守护线程
 * @author dongyazhuo
 */
public class mm_login_guard implements Runnable {
	private String TAG = mm_login_guard.class.getSimpleName() + " : ";

	private mm_login _mm_login = null;

	private mm_cache _mm_cache = null;

	private mm_config _mm_config = null;

	private mm_reactor _mm_reactor = null;

	private String _user;// 用户名
	private String _pwd;// 密码

	public mm_login_guard(mm_reactor _mm_reactor) {
		super();
		this._mm_reactor = _mm_reactor;
		this._mm_login = _mm_reactor._mm_login;
		this._mm_cache = _mm_reactor._mm_cache;
		this._mm_config = _mm_reactor._mm_config;
		this._user = _mm_config.getSet().getAccount_12306().getUser();
		this._pwd = _mm_config.getSet().getAccount_12306().getPwd();
		TAG += _mm_config.getSet().getAccount_12306().getUser() + " : ";
	}

	/**
	 * 检查用户登录动作
	 */
	private void action() {
		// 检查用户登录, 检查间隔为2分钟
		while (true) {
			if (!_mm_config.isStart()) {
				// 结束登陆守护线程
				mm_logger.w(TAG + "config file is changed , start is false !!!");
				mm_start.stop(_mm_reactor);
			}
			login_change();
			mm_thread.sleep(100);
			_mm_login.check_sleep_time();
			long l = _mm_cache.get(_mm_cache.user_time);
			if (l < -1 || l == 0) {// 说明已经过期
				check_user();
			}
		}
	}

	/**
	 * 配置文件修改了登陆用户，重新登陆
	 */
	private void login_change() {
		String user = _mm_config.getSet().getAccount_12306().getUser();
		String pwd = _mm_config.getSet().getAccount_12306().getPwd();
		if (!_user.equals(user) || !_pwd.equals(pwd)) {
			mm_logger.i(TAG + "config file user or pwd is changed!!!");
			_mm_login.login();
		}
	}

	private void check_user() {
		Map<String, Object> url = _mm_config.getUrl_config().get("check_user_url");
		Map<String, Object> data = new HashMap<>();
		data.put("_json_att", "");
		HashMap<String, Object> result = (HashMap) _mm_login.get_mm_http_client().send(url, data);
		Object result_data = result.get("data");
		if (null != result_data) {
			HashMap<String, Object> result_data_map = JSON.parseObject(result_data.toString(), HashMap.class);
			boolean check_user_flag = mm_parse.parse_bool(result_data_map.get("flag"));

			long time_out = get_time_out();
			if (check_user_flag) {
				_mm_cache.set(_mm_cache.user_time, time_out);
			} else {
				String messages = mm_parse.parse_str(result.get("messages"));
				mm_logger.w(TAG + String.format(mm_constant.LOGIN_SESSION_FAIL, messages));
				_mm_login.login();
			}
		}
	}

	private long get_time_out() {
		return 18 * 1000L;
	}

	@Override
	public void run() {
		if (_mm_config.getSet().getAccount_12306().isQuery()) {
			mm_logger.i(TAG + "login_mode is query.");
			return;
		}
		action();
	}
}
