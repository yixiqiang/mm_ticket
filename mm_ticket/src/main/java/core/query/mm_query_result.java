package core.query;

/**
 * @author dongyazhuo
 */
public class mm_query_result {

	public String secretStr;
	public String train_no;
	public String stationTrainCode;
	public String train_date;
	public String query_from_station_name;
	public String query_to_station_name;
	public int seat;
	public String leftTicket;
	public String train_location;
	public int code;
	public int is_more_ticket_num;
	public String cdn;
	public boolean status = false;

	public mm_query_result(String secretStr, String train_no, String stationTrainCode, String train_date,
			String query_from_station_name, String query_to_station_name, int seat, String leftTicket, String train_location,
			int code, int is_more_ticket_num, String cdn, Boolean status) {
		super();
		this.secretStr = secretStr;
		this.train_no = train_no;
		this.stationTrainCode = stationTrainCode;
		this.train_date = train_date;
		this.query_from_station_name = query_from_station_name;
		this.query_to_station_name = query_to_station_name;
		this.seat = seat;
		this.leftTicket = leftTicket;
		this.train_location = train_location;
		this.code = code;
		this.is_more_ticket_num = is_more_ticket_num;
		this.cdn = cdn;
		this.status = status;
	}

	public mm_query_result() {
		super();
	}

	@Override
	public String toString() {
		return "mm_query_result [secretStr=" + secretStr.substring(0, 5) + ", train_no=" + train_no + ", stationTrainCode="
				+ stationTrainCode
				+ ", train_date=" + train_date + ", query_from_station_name=" + query_from_station_name
				+ ", query_to_station_name=" + query_to_station_name + ", seat=" + seat + ", leftTicket=" + leftTicket
				+ ", train_location=" + train_location + ", code=" + code + ", is_more_ticket_num=" + is_more_ticket_num
				+ ", cdn=" + cdn + ", status=" + status + "]";
	}

}
