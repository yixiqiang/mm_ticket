package core.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.alibaba.fastjson.JSON;

import config.mm_config;
import core.net.mm_http_client;
import core.reactor.mm_reactor;
import log.mm_logger;

/**
 * 通讯录
 * @author dongyazhuo
 */
public class mm_address_book {

	private String TAG = mm_address_book.class.getSimpleName() + " : ";

	private mm_http_client _mm_http_client;
	private mm_config _mm_config;
	private mm_reactor _mm_reactor;
	// 乘车人列表
	private Set<HashMap<String, Object>> user_list = new HashSet<HashMap<String, Object>>();
	// 12306上的联系人列表
	private ArrayList normal_passengers = null;

	public mm_address_book(mm_reactor _mm_reactor) {
		super();
		this._mm_reactor = _mm_reactor;
		this._mm_http_client = _mm_reactor._mm_http_client;
		// this._mm_cache = _mm_reactor._mm_cache;
		this._mm_config = _mm_reactor._mm_config;
	}

	/**
	 * 获取全部联系人
	 * @return
	 */
	public ArrayList get_normal_passengers(boolean is_cache) {

		if (null == normal_passengers && is_cache) {
			mm_logger.w(TAG + "NODE:从缓存获取的联系人,如果新添加，请重启程序。");
			return normal_passengers;
		}

		do {
			HashMap r = get_address_book();
			if (null != r.get("data")) {
				HashMap data = JSON.parseObject(r.get("data").toString(), HashMap.class);
				if (null != data && data.size() > 0) {
					if (null != data.get("normal_passengers")) {
						this.normal_passengers = JSON.parseObject(data.get("normal_passengers").toString(), ArrayList.class);
						break;
					} else {
						mm_logger.w(TAG + data);
						if (null != data.get("noLogin")) {// 如果检测到用户未登陆。立即登陆并重试
							if (Boolean.parseBoolean(data.get("noLogin").toString())) {
								_mm_reactor._mm_cache.set(_mm_reactor._mm_cache.user_time, 1);
								// mm_thread.sleep(100);
								throw new RuntimeException();
							}
						}
					}
				}
			}
		} while (true);

		List<String> peoples = _mm_config.getSet().getTicke_peoples();
		for (Object object : normal_passengers) {
			HashMap<String, Object> info = JSON.parseObject(object.toString(), HashMap.class);
			if (peoples.contains(info.get("passenger_name"))) {
				user_list.add(info);
				mm_logger.i(TAG + " 查询到乘车人的信息有 : " + info);
			}
		}
		return normal_passengers;
	}

	private HashMap get_address_book() {
		Map<String, Object> map = _mm_config.getUrl_config().get("get_passengerDTOs");
		Map<String, Object> p = new HashMap<>();
		p.put("_json_att", "");
		HashMap r = (HashMap) _mm_http_client.send(map, p);
		return r;
	}

	/**
	 * 获取乘车人列表
	 * @return
	 */
	public Set<HashMap<String, Object>> get_user_list(boolean is_cache) {

		if (user_list.size() > 0 && is_cache) {
			mm_logger.w(TAG + "NODE:从缓存获取的联系人,如果新添加，请重启程序。");
			return user_list;
		}
		ArrayList normal_passengers = get_normal_passengers(is_cache);
		List<String> peoples = _mm_config.getSet().getTicke_peoples();
		for (Object object : normal_passengers) {
			HashMap<String, Object> info = JSON.parseObject(object.toString(), HashMap.class);
			if (peoples.contains(info.get("passenger_name"))) {
				user_list.add(info);
				mm_logger.i(TAG + " 查询到乘车人的信息有 : " + info);
			}
		}
		return user_list;
	}

	/**
	 * 获取全部联系人用户名
	 * @return
	 */
	public List<String> get_user_names() {
		ArrayList normal_passengers = get_normal_passengers(false);
		List<String> user_names = new ArrayList<>();
		if (null != normal_passengers && normal_passengers.size() > 0) {
			for (Object object : normal_passengers) {
				HashMap user = JSON.parseObject(object.toString(), HashMap.class);
				String passenger_name = user.get("passenger_name").toString();
				user_names.add(passenger_name);
			}
		}
		return user_names;
	}
}
