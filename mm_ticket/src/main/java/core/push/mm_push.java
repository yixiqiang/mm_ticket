package core.push;

import core.reactor.mm_reactor;
import utils.mm_thread;

/**
 * 推送
 * @author dongyazhuo
 */
public class mm_push {

	private String order_no;
	private mm_reactor _mm_reactor;

	public mm_push(String order_no, mm_reactor _mm_reactor) {
		super();
		this.order_no = order_no;
		this._mm_reactor = _mm_reactor;
	}

	public void push() {
		boolean open = _mm_reactor._mm_config.getEmail_conf().isOpen();
		if (open) {
			mm_mail _mm_mail = new mm_mail(_mm_reactor);
			for (int i = 0; i < 5; i++) {
				_mm_mail.push(order_no);
				mm_thread.sleep(2000);
			}
		}
	}
}
