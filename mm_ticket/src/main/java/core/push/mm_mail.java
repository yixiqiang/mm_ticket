package core.push;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import config.mm_config_email_conf;
import core.reactor.mm_reactor;

/**
 * 邮件推送
 * @author dongyazhuo
 */
public class mm_mail {

	mm_reactor _mm_reactor = null;

	mm_config_email_conf email_conf = null;

	private String order_no;

	public mm_mail(mm_reactor _mm_reactor) {
		super();
		this._mm_reactor = _mm_reactor;
		this.email_conf = _mm_reactor._mm_config.getEmail_conf();
	}

	public void push(String order_no) {
		this.order_no = order_no;
		try {
			send();
		} catch (Exception e) {
		}
	}

	private void send() throws Exception {
		// 1、连接邮件服务器的参数配置
		Properties props = new Properties();
		// 设置用户的认证方式
		props.setProperty("mail.smtp.auth", "true");
		// 设置传输协议
		props.setProperty("mail.transport.protocol", "smtp");
		// 设置发件人的SMTP服务器地址
		props.setProperty("mail.smtp.host", email_conf.getHost());
		// 2、创建定义整个应用程序所需的环境信息的 Session 对象
		Session session = Session.getInstance(props);
		// 设置调试信息在控制台打印出来
		session.setDebug(false);
		// 3、创建邮件的实例对象
		Message msg = getMimeMessage(session);
		// 4、根据session对象获取邮件传输对象Transport
		Transport transport = session.getTransport();
		// 设置发件人的账户名和密码
		transport.connect(email_conf.getUsername(), email_conf.getPassword());
		// 发送邮件，并发送到所有收件人地址，message.getAllRecipients() 获取到的是在创建邮件对象时添加的所有收件人, 抄送人, 密送人
		transport.sendMessage(msg, msg.getAllRecipients());

		// 如果只想发送给指定的人，可以如下写法
		// transport.sendMessage(msg, new Address[]{new InternetAddress("xxx@qq.com")});

		// 5、关闭邮件连接
		transport.close();
	}

	public MimeMessage getMimeMessage(Session session) throws Exception {
		// 创建一封邮件的实例对象
		MimeMessage msg = new MimeMessage(session);
		// 设置发件人地址

		msg.setFrom(new InternetAddress(email_conf.getEmail()));
		/**
		 * 设置收件人地址（可以增加多个收件人、抄送、密送），即下面这一行代码书写多行 MimeMessage.RecipientType.TO:发送 MimeMessage.RecipientType.CC：抄送
		 * MimeMessage.RecipientType.BCC：密送
		 */
		msg.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(email_conf.getNotice_email()));
		// 设置邮件主题
		msg.setSubject("mm_java_12306_ticket_订票完成通知", "UTF-8");
		// 设置邮件正文
		msg.setContent(get_content(), "text/html;charset=UTF-8");
		// 设置邮件的发送时间,默认立即发送
		msg.setSentDate(new Date());

		return msg;
	}

	private String get_content() {
		String from_station = _mm_reactor._mm_config.getSet().getFrom_station();
		String to_station = _mm_reactor._mm_config.getSet().getTo_station();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 5; i++) {
			sb.append("<span style=\"font-size:20px\">");
			String content = "QQ : 416331017  提醒您 ：您订购的 " + from_station + " 到  " + to_station + " 预定完成,订单号：" + order_no
					+ " ,请尽快登陆&nbsp&nbsp<a href=\"https://www.12306.cn/index/index.html\">12306官方网站</a>&nbsp&nbsp完成支付或处理 !!! ";
			sb.append(content + "<br>");
			sb.append("</span>");
		}
		return sb.toString();
	}

}
