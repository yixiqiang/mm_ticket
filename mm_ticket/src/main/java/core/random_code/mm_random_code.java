package core.random_code;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;

import config.mm_config;
import core.net.mm_http_client;
import core.reactor.mm_reactor;
import log.mm_logger;
import utils.mm_ruokuai;
import utils.mm_thread;

/**
 * 验证码
 * @author dongyazhuo
 */
public class mm_random_code {

	private String TAG = mm_random_code.class.getSimpleName() + " : ";

	private mm_http_client _mm_http_client;

	private mm_config _mm_config = null;

	private mm_ruokuai _mm_ruokuai = null;

	private Map<String, Long> last_request = new HashMap<String, Long>();

	public mm_random_code(mm_reactor _mm_reactor) {
		super();
		this._mm_http_client = _mm_reactor._mm_http_client;
		this._mm_config = _mm_reactor._mm_config;
		this._mm_ruokuai = _mm_reactor._mm_ruokuai;
		TAG += _mm_config.getSet().getAccount_12306().getUser() + " : ";
	}

	private void sleep(String type) {
		Long long1 = last_request.get(type);
		if (null == long1) {
			return;
		}
		long diff = long1 - System.currentTimeMillis();
		if (null != long1 && diff > 0) {
			mm_thread.sleep(diff);
		}
	}

	/**
	 * 获取一个新的验证码
	 * @param is_login
	 */
	public void get_pass_code_new(boolean is_login) {
		Map<String, Object> url = null;
		if (is_login) {
			url = _mm_config.getUrl_config().get("getCodeImg");
			sleep("getCodeImg");
		} else {
			url = _mm_config.getUrl_config().get("codeImgByOrder");
			sleep("codeImgByOrder");
		}
		url.put("req_url", url.get("req_url") + "" + Math.random());
		Object r = _mm_http_client.send(url, Collections.emptyMap());
		String type = is_login ? "getCodeImg" : "codeImgByOrder";
		last_request.put(type, System.currentTimeMillis() + 5000L);
		try {
			if (r instanceof HttpEntity) {
				HttpEntity rr = (HttpEntity) r;
				String file_path = mm_ruokuai.get_file_path(_mm_config, is_login);
				File f = new File(file_path);
				OutputStream outstream = null;
				outstream = new FileOutputStream(f);
				rr.writeTo(outstream);
				outstream.close();
				mm_logger.i(TAG + "get new pass code finish && saved local !!!");
			} else {
				mm_logger.e(TAG + r);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// RuoKuai.httpPostImage(url, param, data);
	}

	/**
	 * 登陆验证码检查
	 * @param xy
	 * @return
	 */
	public boolean login_code_check() {
		// 请求第三方打码平台，获得验证码
		String xy = _mm_ruokuai.get_code_xy(true);
		Map<String, Object> map = _mm_config.getUrl_config().get("codeCheck");
		Map<String, Object> data = new HashMap<>();
		data.put("answer", xy);
		data.put("rand", "sjrand");
		data.put("login_site", "E");
		HashMap r = (HashMap) _mm_http_client.send(map, data);
		mm_logger.i(TAG + "code_check http result : " + r);
		if (null != r && r.size() > 0) {
			if (null == r.get("result_code")) {
				return false;
			}
			return 4 == Integer.parseInt(r.get("result_code").toString());
		}
		return false;
	}

}
