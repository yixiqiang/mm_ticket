package core.reactor;

import config.mm_config;
import core.cache.mm_cache;
import core.commit.mm_commit;
import core.commit.mm_query_order_complete;
import core.login.mm_login;
import core.login.mm_login_guard;
import core.net.mm_http_client;
import core.query.mm_address_book;
import core.query.mm_query;
import core.random_code.mm_random_code;
import utils.mm_ruokuai;
import utils.mm_thread;

/**
 * 一个处理器,一个处理器对应一个配置文件,对应一个用户
 * @author dongyazhuo
 */
public class mm_reactor implements Runnable {

	// 配置器
	public mm_config _mm_config;
	// 初始化一个请求器
	public mm_http_client _mm_http_client = null;// new mm_http_client();
	// 缓存器
	public mm_cache _mm_cache = null;// new mm_cache();
	// 打码器
	public mm_ruokuai _mm_ruokuai = null;// new mm_ruokuai(this);
	// 验证码识别器
	public mm_random_code _mm_random_code = null;
	// 登陆器
	public mm_login _mm_login = null;// new mm_login(this);
	// 登陆守护
	public mm_login_guard _mm_login_guard = null;// new mm_login_guard(this);
	// 联系人
	public mm_address_book _mm_address_book = null;// new mm_address_book(this);
	// 查票
	public mm_query _mm_query = null;// new mm_query(this);
	// 提交器
	public mm_commit _mm_commit = null;
	// 订单排队查询
	public mm_query_order_complete _mm_query_order_complete = null;

	public mm_thread login_guard_thread;

	public mm_thread mm_query_thread;

	public mm_reactor(mm_config _mm_config) {
		super();
		this._mm_config = _mm_config;
		init();
	}

	private void init() {
		_mm_http_client = new mm_http_client();
		_mm_cache = new mm_cache();
		_mm_ruokuai = new mm_ruokuai(this);
		_mm_random_code = new mm_random_code(this);
		_mm_login = new mm_login(this);
		_mm_login_guard = new mm_login_guard(this);
		_mm_address_book = new mm_address_book(this);
		_mm_commit = new mm_commit(this);
		_mm_query = new mm_query(this);
		_mm_query_order_complete = new mm_query_order_complete(this);
		login_guard_thread = new mm_thread();
		mm_query_thread = new mm_thread();
	}

	private void action() {
		// 登陆
		_mm_login.login();
		// 延迟启动登陆守护线程
		login_guard_thread.start(_mm_login_guard, 2);
		// 查票
		mm_query_thread.start(_mm_query);
	}

	@Override
	public void run() {
		action();
	}

}
