package mm_ticket;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.LinkedHashMap;

import org.yaml.snakeyaml.Yaml;

import config.mm_config_station_name;

public class mm_config_test {

	public static void main(String[] args) throws IOException {
		Yaml yaml = new Yaml();
		InputStream in = mm_config_station_name.class.getResourceAsStream("/test.yml");
		LinkedHashMap map = yaml.loadAs(in, LinkedHashMap.class);
		System.err.println(map);
		map.put("test_yml", false);
		// String dump = yaml.dump(map);
		Writer output = new FileWriter(new File("test.yml"));
		output.flush();
		output.close();

	}
}
