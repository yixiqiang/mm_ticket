# mm_java_12306_ticket

#### 介绍

    当前版本（0.25）

    12306火车购票软件的一个java多用户并发版本。
    优点：
    1、采用reator反应堆模式，支持多用户同时在线购票。
    2、每用户独立配置文件。
    3、实时配置文件更新，不需要重新启动程序。（正在开发，预计0.3版本发布）
    4、邮件、短信、电话、web_hook。（邮件通知完成）

#### 版本记录
	 0.25
	1、修复快速查询等若干bug。
	 0.2
	1、修复邮件通知、订票完成线程结束等若干bug。
	2、增加cdn持续检测功能，优化预定查询速度。
	 0.1
	1、完成基础框架搭建，可执行预定功能。
	
#### 软件架构
    程序采用jdk8开发，未来计划兼容1.6和1.7
    
#### 流程图
![流程图](https://liwuqu-1257688354.cos.ap-chengdu.myqcloud.com/mm_ticket/liuchengtu.png)
#### 模型图：
![模型图](https://liwuqu-1257688354.cos.ap-chengdu.myqcloud.com/mm_ticket/moxing.png)
#### 目录结构：
![目录结构](https://liwuqu-1257688354.cos.ap-chengdu.myqcloud.com/mm_ticket/mulu_jiegou.png)

#### 安装教程（学习开发人员用）

	1. 依赖：
		org.yaml.snakeyaml
		com.alibaba.fastjson
		org.apache.httpcomponents.httpclient
		javax.mail.mail
		思路为越少依赖包越好，每次版本更新。可能会对包进行修改。
	2. 修改resource目录下ticket_config的配置文件。
	3. run.mm_application.main() 启动。 

#### 使用说明（仅使用）
	如果你只是想使用订票功能，那么上述的安装环境不需要关心。操作下面4步骤就可以。
	1、安装java jre 8.
	2、下载最新版本。
	3、修改配置文件 config目录的，按照格式要求填写相关信息。
	4、使用命令运行 java  -jar  mm_ticket_0.1.jar  -c  config
[mm_ticket_ 下载](https://liwuqu-1257688354.cos.ap-chengdu.myqcloud.com/mm_ticket/version/mm_ticket_0.25.rar)。

	Note：参数-c 为配置文件所在的目录，绝对目录，相对目录都可以。 

![运行示意](https://liwuqu-1257688354.cos.ap-chengdu.myqcloud.com/mm_ticket/run.png)
![运行示意1](https://liwuqu-1257688354.cos.ap-chengdu.myqcloud.com/mm_ticket/run2.png)
![运行示意1](https://liwuqu-1257688354.cos.ap-chengdu.myqcloud.com/mm_ticket/youjian.png)


#### 参与贡献

	1. 欢迎大家fork与star，互相学习交流
	2. qq 416331017  欢迎交流学习。
	3. 目前未做功能有：短信、电话推送，配置文件实时更新，后期及时发布。欢迎订阅。也希望大家多多push request。
	4. 谢谢。

